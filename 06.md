# Reduce infrastructure cost by optimization and modernization

## Optimization
- Utilize spot instances
- Rightsizing underutilize instance
- Review unused and idle resources (ELB, EC2, Cloudwatch, etc.)
... and many more

## Modernization
- Utilize containerization (ECS & EKS)
- Infrastructure as a code (Terraform, Ansible, etc.)
... and many more
